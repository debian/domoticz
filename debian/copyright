Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: domoticz
Source: https://github.com/domoticz/domoticz
Files-Excluded:
 hardware/1Wire/OneWireDomoticzService/OneWireDomoticzService/*.dll
 msbuild/WindowsInstaller/*.dll
 msbuild/WindowsInstaller/*.exe
 tools/*.dll
 tools/*.exe

Files: *
Copyright: 2018 Rob Peters (gizmocuz)
           2018 gaudryc
           2018 dnpwwo
           2018 Danny Bloemendaal <danny@bloemeland.nl>
           2018 Joost van den Broek
           2018 other authors
Comment: See https://github.com/domoticz/domoticz/graphs/contributors
License: GPL-3+

Files: hardware/Evohome*
Copyright: 2018 fullTalgoRythm
License: GPL-3+

Files: hardware/Gpio*
Copyright: 2018 Joost van den Broek
License: GPL-3+

Files: hardware/libusbwinusbbridge.h
Copyright: 2018 Adam Kropelin
License: GPL-3+

Files: tinyxpath/*
Copyright: 2002-2004 Yves Berquin (yvesb@users.sourceforge.net)
License: zlib/libpng

Files: debian/*
Copyright: 2018 Federico Ceratto <federico@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation, either version 3 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in /usr/share/common-licenses/GPL-3.

License: zlib/libpng
   This software is provided 'as-is', without any express or implied
   warranty. In no event will the authors be held liable for any
   damages arising from the use of this software.
   .
   Permission is granted to anyone to use this software for any
   purpose, including commercial applications, and to alter it and
   redistribute it freely, subject to the following restrictions:
   .
   1. The origin of this software must not be misrepresented; you must
   not claim that you wrote the original software. If you use this
   software in a product, an acknowledgment in the product documentation
   would be appreciated but is not required.
   .
   2. Altered source versions must be plainly marked as such, and
   must not be misrepresented as being the original software.
   .
   3. This notice may not be removed or altered from any source
   distribution.
